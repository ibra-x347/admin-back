import os
import logging

from logging.handlers import TimedRotatingFileHandler

logger = logging.getLogger()


# init logging
def init_logging(prefix_log='', enable_console_handler: bool=True):
    logging.getLogger().setLevel(logging.NOTSET)
    # path of the current file
    # path = os.path.dirname(os.path.abspath(__file__))
    # logs relative directory
    logs_abs_path = 'logs'
    # logs filename
    service_file_name = '%sservice.log' % prefix_log
    access_file_name = '%saccess.log' % prefix_log
    error_file_name = '%serror.log' % prefix_log

    # create logs directory
    if not os.path.exists(logs_abs_path):
        os.mkdir(logs_abs_path)

    # setup log formatter
    log_formatter = logging.Formatter(u'%(asctime)s - %(levelname)s - %(filename)s[LINE:%(lineno)d] - %(message)s')

    # service log with rotate once a day
    service_file_handler = TimedRotatingFileHandler(os.path.join(logs_abs_path, service_file_name), when="d", interval=1, backupCount=30)
    service_file_handler.setFormatter(log_formatter)
    logger.setLevel(logging.INFO)
    logger.addHandler(service_file_handler)

    # error log
    error_file_handler = logging.FileHandler(os.path.join(logs_abs_path, error_file_name))
    error_file_handler.setFormatter(log_formatter)
    error_file_handler.setLevel(logging.ERROR)
    logger.addHandler(error_file_handler)

    # Logging service messages into console
    if enable_console_handler:
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(log_formatter)
        console_handler.setLevel(logging.NOTSET)
        logger.addHandler(console_handler)
