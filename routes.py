from aiohttp.web_app import Application
from aiogmqtt import MqttApplication


mqtt_routes = [
]


from core.controller import DevicesController, WidgetGroupController

http_routse = [
    ('GET',     'devices/all',          DevicesController),
    ('GET',     'widgets/group/{group_id}',        WidgetGroupController),
    # ('GET',     'widgets/unit',     DevicesController),
]


# setup routes
def setup_routes(web_app: Application, mqtt_app: MqttApplication, base_path: str):
    # http
    for route in http_routse:
        web_app.router.add_route(route[0], f'{base_path}{route[1]}', route[2])

    # mqtt
    for route in mqtt_routes:
        mqtt_app.add_route(route[0], f'{base_path}{route[1]}', route[2])

