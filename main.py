import logging
import asyncio
from dataclasses import dataclass
from aiohttp import web
from aiogmqtt import MqttApplication, MqttAppCfg, LOG_LVL

from log import init_logging
from routes import setup_routes
from common.state import State, StateCfg


# add headers for response
async def on_prepare(request, response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'X-AccessToken,content-type'
    response.headers['Access-Control-Allow-Methods'] = 'OPTIONS,GET,POST,PUT,DELETE'


@dataclass
class _Settings:
    token: str = '30ciyMU52twgu44DtHItBBIolPVaurnnhqtG0dzTxTXPNSg4pMnOdT1oyMnQ1A38'
    hostname: str = '127.0.0.1'
    port: int = 9090
cfg = _Settings()


if __name__ == "__main__":
    init_logging()
    logger = logging.getLogger()

    loop = asyncio.get_event_loop()

    # create web application (with set debug)
    web_app = web.Application(
        middlewares={},
        client_max_size=1024 ** 2 * 64,
    )
    # add Headers for request
    web_app.on_response_prepare.append(on_prepare)

    # MQtt-Application
    mqtt_app = MqttApplication(
        name='name1',
        session_name='session-1',
        app_cfg=MqttAppCfg(
            url='mqtt.flespi.io',
            mqtt_token=cfg.token,
            base_path=f'',
        ),
        log_lvl=LOG_LVL.STANDARD,
    )
    setup_routes(web_app, mqtt_app, '/')

    state = State()
    state.install_routes(mqtt_app=mqtt_app)

    mqtt_app.on_startup.append(state.on_startup)
    mqtt_app.on_shutdown.append(state.on_shutdown)

    web_app.on_startup.append(mqtt_app.run_wait)
    web_app.on_shutdown.append(mqtt_app.stop)

    web_app.main_cfg = cfg
    mqtt_app.main_cfg = cfg

    # run web application
    try:
        web.run_app(
            web_app,
            host=cfg.hostname,
            port=cfg.port,
            shutdown_timeout=2,
        )
    finally:
        loop.close()
