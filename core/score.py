from dataclasses import dataclass
import ujson


@dataclass
class CALC_TYPES:
    trips = 'Trips'
    speed = 'Speed'
    acceleration = 'Acceleration'
    cornering = 'Cornering'
    braking = 'Braking'


FINES = {
    CALC_TYPES.speed: {
        'type': 'Speed',
        'value': [
            {'min': 70, 'max': 80, 'value': 2, 'field': 'speed_max'},
            {'min': 81, 'max': 90, 'value': 3, 'field': 'speed_max'},
            {'min': 91, 'max': None, 'value': 5, 'field': 'speed_max'}
        ]
    },
    CALC_TYPES.acceleration: {
        'type': 'Acceleration',
        'value': 3
    },
    CALC_TYPES.cornering: {
        'type': 'Cornering',
        'value': 3
    },
    CALC_TYPES.braking: {
        'type': 'Braking',
        'value': 3
    },
}


def getTripScore(intervals_by_types):
    result = {
        'score': 100,
        # 'violations': []
    }

    for i_type in intervals_by_types:
        score_schema = FINES.get(i_type)
        if score_schema:
            intervals = intervals_by_types[i_type]
            print(intervals)

            for interval in intervals:
                pp_val = interval['ppValue'] or score_schema['value']
                if isinstance(pp_val, int):
                    interval['penalty_point'] = pp_val
                    result['score'] -= pp_val
                    # result['violations'].push(interval)
                else:
                    if isinstance(pp_val, str):
                        pp_val = ujson.loads(pp_val)
                    print(pp_val)
                    assert isinstance(pp_val, list)
                    for val in pp_val:
                        if val['min'] < interval[val['field']] and (not val['max'] or val['max'] > interval[val['field']]):
                            interval['penalty_point'] = pp_val
                            result['score'] -= val['value']
                            # result['violations'].push(interval)
    return result

#               if (this.isNumeric(scoreSchema.value)) {
#                 const penalty = item.ppValue || scoreSchema.value
#                 item.penaltyPoint = penalty
#                 result.score = result.score - penalty
#                 result.violations.push(item)
#               } else {
#                 const penalty = item.ppValue ? JSON.parse(item.ppValue) : scoreSchema.value
#
#                 penalty.forEach(scoreItem => {
#                   if (scoreItem.min < item[scoreItem.field] && (!scoreItem.max || scoreItem.max > item[scoreItem.field])) {
#                     item.penaltyPoint = scoreItem.value
#                     result.score = result.score - scoreItem.value
#                     result.violations.push(item)
#                   }
#                 })
#               }


