from aiohttp import web, ClientSession, ClientResponse, TCPConnector
from common.state import State
from dataclasses import dataclass
from time import time
import ujson
from .score import CALC_TYPES, FINES, getTripScore

# 0 = unknown group
GROUPS = {
    1: 'Driven group',
    2: 'Minivan',
    3: 'Taxi',
    4: 'Green car',
}

# l per100 km
DEFAULT_FUEL_CONSUMPTION = 9
AVG_FUEL_CONSUMPTION = {
    1289937: 13,
    904489: 11,
    1298985: 8.5,
    1283669: 16.5,
}

# co2 per100 km
DEFAULT_CO2 = 9.5
AVG_CO2 = {
    1289937: 800,
    904489: 750,
    1298985: 1100,
    1283669: 1200,
}

# co2 per100 km
DEFAULT_MOBILE_USAGE = 14
MOBILE_USAGE = {
    1289937: 27,
    904489: 3,
    1298985: 28,
    1283669: 47,
}


DRIVERS = {
    'Alex': set([1289937, 904489]),
    'John': set([1298985, 1283669])
}


def get_driver(device_id: int) -> str:
    for dr in DRIVERS:
        if device_id in DRIVERS[dr]:
            return dr
    return 'Unknown'


class DevicesController(web.View):
    async def get(self):
        result = dict(
            devices=dict(),
            byGroup=dict(),
            groups=GROUPS,
        )

        for i in State().devices.data:
            item = State().devices.data[i].copy()     #type: dict
            pk = item['id']
            gr_id = 0 if not item.get('gId') or not GROUPS.get(item['gId']) else item['gId']
            result['devices'][pk] = item
            result['byGroup'].setdefault(gr_id, [])
            result['byGroup'][gr_id].append(pk)

            item['driver'] = get_driver(pk)

            result['devices'][pk] = item

        print(result['devices'].keys())
        print(result['byGroup'])

        device_ids = [str(device_id) for device_id in result['devices'].keys()]
        # trips by device-id
        trips = {}

        session = ClientSession(connector=TCPConnector())
        # resp = await session.get(
        #     url=f'https://flespi.io/gw/calcs/name=Trips/devices/{",".join(devices)}/intervals/all',
        #     headers={
        #         'Authorization': self.request.app.main_cfg.token
        #     },
        #     params={'count': '1', 'reverse': 'true', 'begin': str(time_from), 'end': str(time_to)}
        # )
        # print(await resp.json())
        #
        # await session.close()

        # -- get last trips
        response = await session.get(
            url=f'https://flespi.io/gw/calcs/name={CALC_TYPES.trips}/devices/{",".join(device_ids)}/intervals/all',
            headers={
                'Authorization': self.request.app.main_cfg.token
            },
            params=dict(data=ujson.dumps({'reverse': True, 'count': 1}, ))
        )
        data = await response.json()
        for interval in data.get('result') or []:
            device_id = interval['device_id']
            trips[device_id] = dict(
                begin=interval['begin'],
                end=interval['end'],
                active=interval['active'],
                duration=interval['duration'],
                mileage=round(interval['mileage'], 2),
                # co2=round(AVG_CO2.get(device_id) or DEFAULT_CO2 / 100 * interval['mileage'], 2)
                co2=round(AVG_CO2.get(device_id) or DEFAULT_CO2 / 100, 2)
            )
            result['devices'][device_id]['trip'] = trips[device_id]

        for device_id in trips:
            trip = trips[device_id]
            response = await session.get(
                url=f'https://flespi.io/gw/calcs/all/devices/{",".join(device_ids)}/intervals/all',
                headers={
                    'Authorization': self.request.app.main_cfg.token
                },
                params=dict(data=ujson.dumps({'reverse': True, 'begin': trip['begin'], 'end': trip['end']},))
            )
            data = await response.json()

            score = 100
            intervals = data.get('result') or []

            for interval in intervals:
                i_type = interval['type']
                score_schema = FINES.get(i_type)
                if score_schema:
                    if not (trip['begin'] <= interval['begin'] and trip['end'] >= interval['end']):
                        continue

                    pp_val = interval['ppValue'] or score_schema['value']
                    if isinstance(pp_val, int):
                        interval['penalty_point'] = pp_val
                        score -= pp_val
                    else:
                        if isinstance(pp_val, str):
                            pp_val = ujson.loads(pp_val)
                        assert isinstance(pp_val, list)
                        for val in pp_val:
                            if val['min'] < interval[val['field']] and (
                                    not val['max'] or val['max'] > interval[val['field']]):
                                interval['penalty_point'] = pp_val
                                score -= val['value']
            trip['score'] = score

        await session.close()

        return web.json_response(data=dict(
            result=result,
            # errors=[]
        ))


@dataclass
class GROUP_WIDGETS:
    general = 'general'
    mileage = 'mileage'
    trips = 'trips'
    score = 'score'
    eco = 'eco'
    driver_performance = 'driver_performance'
    violations = 'violations'
    mobile_usage = 'mobile_usage'
    car = 'car'


class WidgetGroupController(web.View):
    async def get(self):
        current_time = int(time())
        group_id = int(self.request.match_info['group_id'])
        time_from = int(self.request.query.get('from') or current_time - 60 * 60 * 24 * 30)
        time_to = int(self.request.query.get('to') or current_time)
        assert GROUPS[group_id] and time_from and time_to

        # data by widget-name
        result, errors = dict(), []

        res_by_devices = {}

        # -- get units for group_id
        device_ids = []
        for device_id in State().devices.data:
            device = State().devices.data[device_id]
            gr_id = 0 if not device.get('gId') or not GROUPS.get(device['gId']) else device['gId']
            if gr_id == group_id:
                device_ids.append(str(device_id))
                res_by_devices[device_id] = {
                    'name': device['name'],
                    'driver': get_driver(device_id),
                    'widgets': {}
                    # 'score': 100,
                    # 'violations': 0
                }

        # -- get intervals by devices, {device: {type: [],],}}
        active_ids = set()
        intervals = {}
        async with ClientSession() as session:
            async with session.get(
                # url=f'https://flespi.io/gw/calcs/name=Trips/devices/{",".join(devices)}/intervals/all',
                url=f'https://flespi.io/gw/calcs/all/devices/{",".join(device_ids)}/intervals/all',
                headers={
                    'Authorization': self.request.app.main_cfg.token
                },
                params=dict(data=ujson.dumps({'reverse': True, 'begin': time_from, 'end': time_to},))
            ) as response:
                data = await response.json()
                for interval in data.get('result') or []:
                    device_id = interval['device_id']
                    intervals.setdefault(device_id, {})
                    intervals[device_id].setdefault(interval['type'], [])
                    intervals[device_id][interval['type']].append(interval)

                    if interval['type'] == CALC_TYPES.trips:
                        active_ids.add(device_id)

                errors = data.get('errors') or []

        # for group
        all_mileage = 0
        all_duration = 0
        all_trips = 0
        # {type: count}
        all_violations = {}

        all_score_sum = 0
        max_score = None
        min_score = None

        # -- score
        for device_id in intervals:
            intervals_by_types = intervals[device_id]
            trips = intervals_by_types[CALC_TYPES.trips]
            if not trips:
                continue

            count_trips = len(trips)
            device_mileage = 0
            device_duration = 0
            device_violations = {}
            score_sum = 0

            all_trips += count_trips

            for trip in trips:
                device_mileage += trip['mileage']
                device_duration += trip['duration']
                score = 100
                violations = {}

                for i_type in intervals_by_types:
                    score_schema = FINES.get(i_type)
                    if score_schema:
                        _intervals = intervals_by_types[i_type]
                        violations[i_type] = len(_intervals)

                        device_violations.setdefault(i_type, 0)
                        device_violations[i_type] += violations[i_type]

                        all_violations.setdefault(i_type, 0)
                        all_violations[i_type] += violations[i_type]

                        for interval in _intervals:

                            if not (trip['begin'] <= interval['begin'] and trip['end'] >= interval['end']):
                                continue

                            pp_val = interval['ppValue'] or score_schema['value']
                            if isinstance(pp_val, int):
                                interval['penalty_point'] = pp_val
                                score -= pp_val
                            else:
                                if isinstance(pp_val, str):
                                    pp_val = ujson.loads(pp_val)
                                assert isinstance(pp_val, list)
                                for val in pp_val:
                                    if val['min'] < interval[val['field']] and (
                                            not val['max'] or val['max'] > interval[val['field']]):
                                        interval['penalty_point'] = pp_val
                                        score -= val['value']
                if score < 0:
                    score = 0
                trip['score'] = score
                score_sum += score
                trip['violations'] = violations
                res_by_devices[device_id].setdefault('trips', [])
                res_by_devices[device_id]['trips'].append(trip)
                # res_by_devices[device_id]['moving'] = round(100 / (time_to - time_from) * device_duration, 2)
                # res_by_devices[device_id]['parker'] = 100 - res_by_devices[device_id]['moving']
                max_score = max(score, max_score or score)
                min_score = min(score, min_score) if min_score is not None else score

            # res_by_devices[device_id]['score'] = score if score > 0 else 0
            res_by_devices[device_id]['mileage'] = device_mileage
            res_by_devices[device_id]['duration'] = device_duration
            res_by_devices[device_id]['count_trips'] = count_trips
            res_by_devices[device_id]['score_avg_by_trips'] = score_sum / count_trips
            res_by_devices[device_id]['mileage_avg_by_trips'] = round(device_mileage / count_trips, 2)
            res_by_devices[device_id]['duration_avg_by_trips'] = round(device_duration / count_trips, 2)

            device_moving = round(100 / (time_to - time_from) * device_duration, 2)
            res_by_devices[device_id]['widgets'][GROUP_WIDGETS.trips] = dict(
                count=count_trips,
                avg_trips=None,
                moving=device_moving,
                parker=100 - device_moving,
            )

            count_violations = sum(device_violations.values())
            percents = {}
            for name in device_violations:
                percents[name] = round(100 / count_violations * device_violations[name], 2)

            res_by_devices[device_id]['widgets'][GROUP_WIDGETS.violations] = dict(
                all=count_violations,
                # per_car=count_violations / w_general['count'] if w_general['count'] else 0,
                counts=device_violations,
                percents=percents
            )

            res_by_devices[device_id]['widgets'][GROUP_WIDGETS.mileage] = dict(
                total_mileage=round(device_mileage, 2),
                avg_mileage=round(device_mileage / count_trips, 2) if count_trips else 0
            )

            res_by_devices[device_id]['widgets'][GROUP_WIDGETS.eco] = dict(
                avg_fuel_consumption=AVG_FUEL_CONSUMPTION.get(device_id) or DEFAULT_FUEL_CONSUMPTION,
                avg_co2=AVG_CO2.get(device_id) or DEFAULT_CO2,
            )

            res_by_devices[device_id]['widgets'][GROUP_WIDGETS.mobile_usage] = dict(
                value=MOBILE_USAGE.get(device_id) or DEFAULT_MOBILE_USAGE
            )
            all_mileage += device_mileage
            all_duration += device_duration
            all_score_sum += score_sum

        w_eco = dict(
            avg_fuel_consumption=round(sum([AVG_FUEL_CONSUMPTION[i] for i in AVG_FUEL_CONSUMPTION if i in res_by_devices and 'count_trips' in res_by_devices[i]]) / len(device_ids), 2),
            avg_co2=round(sum([AVG_CO2[i] for i in AVG_CO2 if i in res_by_devices and 'count_trips' in res_by_devices[i]]) / len(device_ids), 2),
        )

        w_mobile_usage = dict(
            value=round(sum([MOBILE_USAGE.get(i) or DEFAULT_MOBILE_USAGE for i in MOBILE_USAGE if i in res_by_devices and 'count_trips' in res_by_devices[i]]) / len(device_ids), 2),
        )

        for device_id in device_ids:
            device_id = int(device_id)
            # if device_id not in res_by_devices:
            score = res_by_devices[device_id].get('score_avg_by_trips')
            res_by_devices[device_id]['widgets'][GROUP_WIDGETS.score] = dict(
                score=round(score, 2) if score else None,
                tariff='Optimal'
            )

            res_by_devices[device_id]['widgets'][GROUP_WIDGETS.car] = dict(
                sec_lvl=80,
                veh_type='Light car',
                number='####',
                year=2000
            )


        all_moving = round(100 / (time_to - time_from) * all_duration, 2)
        w_general = dict(
            count=len(device_ids),
            active=len(active_ids),
            inactive=0,
            moving=all_moving,
            parker=100 - all_moving
        )
        w_general['inactive'] = w_general['count'] - w_general['active']

        w_trips = dict(
            all_trips=all_trips,
            # trips per car
            avg_trips_count=round(all_trips/w_general['count'], 2) if w_general['count'] else 0
        )

        print(11111, all_mileage, all_duration, all_trips)
        w_mileage = dict(
            total_mileage=round(all_mileage, 2),
            total_duration=round(all_duration),
            # km per car
            daily_mileage=round(all_mileage / w_general['count'], 2) if w_general['count'] else 0,
            # km per trip
            avg_mileage=round(all_mileage / all_trips, 2) if all_trips else 0,
        )

        w_score = dict(
            avg_score=round(all_score_sum / all_trips, 2) if all_trips else 0,
            max_score=max_score,
            min_score=min_score,
        )

        count_violations = sum(all_violations.values())
        percents = {}
        for name in all_violations:
            percents[name] = round(100 / count_violations * all_violations[name], 2)
        w_vialtions = dict(
            all=count_violations,
            per_car=count_violations / w_general['count'] if w_general['count'] else 0,
            counts=all_violations,
            percents=percents
        )

        result[GROUP_WIDGETS.general] = w_general
        result[GROUP_WIDGETS.trips] = w_trips
        result[GROUP_WIDGETS.mileage] = w_mileage
        result[GROUP_WIDGETS.score] = w_score
        result[GROUP_WIDGETS.eco] = w_eco
        result[GROUP_WIDGETS.violations] = w_vialtions
        result[GROUP_WIDGETS.mobile_usage] = w_mobile_usage
        result['res_by_devices'] = res_by_devices
        result['name'] = GROUPS[group_id]

        return web.json_response(data=dict(
            result=result,
            errors=errors,
        ))


# # -- get trips by units
# session = ClientSession(connector=TCPConnector())
# resp = await session.get(
#     url=f'https://flespi.io/gw/calcs/name=Trips/devices/{",".join(devices)}/intervals/all',
#     headers={
#         'Authorization': self.request.app.main_cfg.token
#     },
#     params={'count': '1', 'reverse': 'true', 'begin': str(time_from), 'end': str(time_to)}
# )
# print(await resp.json())
#
# await session.close()
