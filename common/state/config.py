from dstate import DStateCfg
from dstate.base import EntityCfg

from .base import E_TYPES, S_TYPES
from .storages import DeviceEntityBase, DeviceStorage


# shemas by entities
class E_SCHEMA:
    __schema = {
        E_TYPES.device: EntityCfg(
            storage_cls=DeviceStorage,
            entity_cls=DeviceEntityBase,
            e_topic='',
            topics=(
                'flespi/state/gw/devices/{id}',
                'flespi/state/gw/devices/{id}/telemetry/{field}',
                'flespi/state/gw/plugins/{plugin_id}/devices/{id}',
            )
        ),
    }

    @classmethod
    def get_by_e_type(cls, e_type: str) -> EntityCfg:
        return cls.__schema[e_type]

    @classmethod
    def get_all(cls) -> dict:
        return cls.__schema


# state-config, class-storage by e-types
class StateCfg(DStateCfg):
    # entity-types
    E_TYPES = E_TYPES
    # storage-types
    S_TYPES = S_TYPES
    # schema topics-entity-storage
    e_schema = E_SCHEMA.get_all()
