from aiogmqtt import MqttApplication, MqttClient
import logging
from dstate import DState

from .storages import DeviceStorage
from .base import E_TYPES
from .config import StateCfg

logger = logging.getLogger()


# state
class State(DState):
    __name__ = 'State'
    """
    :var _mqtt_client: MqttClient
    """

    __slots__ = '_mqtt_client',

    # State-config
    config = StateCfg

    def __init__(self):
        super().__init__()
        self._mqtt_client = None

    async def on_startup(self, mqtt_app: MqttApplication):
        self._mqtt_client = await mqtt_app.get_client(name=self.__name__)
        await super().on_startup(mqtt_app)

    @property
    # mqtt-client
    def mqtt_client(self) -> MqttClient:
        # TODO: fix, wait start
        assert self.__getattribute__('_mqtt_client'), 'mqtt_client is not initialize'
        return self._mqtt_client

    # async def on_startup(self, mqtt_app: MqttApplication):
    #     await super().on_startup(mqtt_app)
    #     print(self.domains.data)

    # -- GETTERS S_TYPES (storage)
    @property
    def devices(self) -> DeviceStorage:
        return self[E_TYPES.device]
    # -- END
