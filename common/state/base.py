from dstate import MD_FIELDS_BASE


# entity-types for saved in mqtt
class E_TYPES:
    device: str = 'device'


# storage-types (mqtt), and topics by e-types
class S_TYPES:
    # types
    flespi: str = 'flespi'


class MD_FIELDS(MD_FIELDS_BASE):
    pass
