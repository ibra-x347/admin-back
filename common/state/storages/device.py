from aiogmqtt import MqttRequest
from dataclasses import dataclass, field

from dstate.base_storage import BaseDictStorage, EDict
from dstate.base import EntityBase

from ..base import E_TYPES, S_TYPES, MD_FIELDS


@dataclass
class DeviceEntityBase(EntityBase):
    name: str = field(metadata={MD_FIELDS.STORAGE_TYPE: S_TYPES.flespi})
    device_type_id: int = field(metadata={MD_FIELDS.STORAGE_TYPE: S_TYPES.flespi})
    # messages_ttl: int = field(metadata={MD_FIELDS.STORAGE_TYPE: S_TYPES.flespi})
    # messages_rotate: int = field(metadata={MD_FIELDS.STORAGE_TYPE: S_TYPES.flespi})
    gId: int = field(metadata={MD_FIELDS.STORAGE_TYPE: S_TYPES.flespi})

    position: EDict = field(metadata={MD_FIELDS.STORAGE_TYPE: S_TYPES.flespi})
    timestamp: int = field(metadata={MD_FIELDS.STORAGE_TYPE: S_TYPES.flespi})

    cid: int

    # device-id
    id: int = field(metadata={MD_FIELDS.STORAGE_TYPE: S_TYPES.flespi})
    # configuration: EDict = field(default_factory=lambda: EDict(), metadata={MD_FIELDS.STORAGE_TYPE: S_TYPES.flespi})

    @property
    # fleet-id
    def fleet_cid(self):
        return self.cid

    @classmethod
    def get_e_type(cls):
        return E_TYPES.device

    @property
    def acc_id(self) -> int:
        return self.cid

    @property
    def cids_by_storage(self) -> dict:
        return {
            S_TYPES.flespi: None,
        }


# storage
class DeviceStorage(BaseDictStorage):
    __name__ = 'DeviceStorage'

    async def on_message(self, request: MqttRequest, **kwargs):
        # print(request.topic, request.payload)
        return await super().on_message(request, **kwargs)
